//
//  SchoolScorePresenter.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation
class SchoolScorePresenter:SchoolScoreViewToPresenterProtocol {
    func fetchschoolScore(with dbn: String) {
        interactor?.fetchschoolScore(with: dbn)
    }
    
    var view: SchoolScorePresenterToViewProtocol?
    
    var interactor: SchoolScorePresenterToInteractorProtocol?
    
    var router: SchoolScorePresenterToRouterProtocol?
    
}

extension SchoolScorePresenter:SchoolScoreInteractorToPresenterProtocol {
    func showNoInterNetAvailabilityMessage() {
        view?.showNoInterNetAvailabilityMessage()
    }
    
    func displayScoreDetails(scoreModel: SchoolScore) {
        view?.updateScores(model: scoreModel)
    }
    
  
}
