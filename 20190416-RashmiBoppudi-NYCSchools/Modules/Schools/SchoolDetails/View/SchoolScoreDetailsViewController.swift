//
//  SchoolScoreDetailsViewController.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import UIKit
import SwiftSpinner

class SchoolScoreDetailsViewController: UIViewController {
    var presenter:SchoolScoreViewToPresenterProtocol?
    @IBOutlet weak var readingAvgScore: UILabel!
    @IBOutlet weak var mathAvgScore: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    var dbn:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SwiftSpinner.useContainerView(view)
        SwiftSpinner.show("Please wait loading Scores of \(dbn).....")
        presenter?.fetchschoolScore(with: dbn)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SchoolScoreDetailsViewController:SchoolScorePresenterToViewProtocol {
    func updateScores(model: SchoolScore) {
        SwiftSpinner.hide()
        readingAvgScore.text = model.satCriticalReadingAvgScore
        mathAvgScore.text = model.satMathAvgScore
        writingAvgScore.text = model.satWritingAvgScore
    }
    
    func showNoInterNetAvailabilityMessage() {
        SwiftSpinner.hide()
        showAlert(title: "No Internet", message: "Please Check you internet connection and try again", actionTitle: "Okay")
    }
    
    func showAlert(title:String,message:String,actionTitle:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
}
