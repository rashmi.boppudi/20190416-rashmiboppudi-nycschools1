//
//  SchoolScoreModel.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation

struct SchoolScore:Codable {
    var dbn:String
    var schoolName:String
    var numOfSatteSttakers:String
    var satCriticalReadingAvgScore :String
    var satMathAvgScore : String
    var satWritingAvgScore: String
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numOfSatteSttakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
    init(dbn: String, name: String,numOfSatteSttakers:String,satCriticalReadingAvgScore:String,satMathAvgScore:String,satWritingAvgScore:String) {
        self.dbn = dbn
        self.schoolName = name
        self.numOfSatteSttakers = numOfSatteSttakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let dbn = try values.decode(String.self, forKey: .dbn)
        let name = try values.decode(String.self, forKey: .schoolName)
        let numOfSatteSttakers = try values.decode(String.self, forKey: .numOfSatteSttakers)
        let satCriticalReadingAvgScore = try values.decode(String.self, forKey: .satCriticalReadingAvgScore)
        let satMathAvgScore = try values.decode(String.self, forKey: .satMathAvgScore)
        let satWritingAvgScore = try values.decode(String.self, forKey: .satWritingAvgScore)
        self.init(dbn: dbn, name: name, numOfSatteSttakers: numOfSatteSttakers, satCriticalReadingAvgScore: satCriticalReadingAvgScore, satMathAvgScore: satMathAvgScore, satWritingAvgScore: satWritingAvgScore)
        
    }
}
