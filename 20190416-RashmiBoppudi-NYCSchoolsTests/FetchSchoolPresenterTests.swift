//
//  FetchSchoolPresenterTests.swift
//  20190416-RashmiBoppudi-NYCSchoolsTests
//
//  Created by Rashmi Boppudi on 17/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import XCTest
@testable import _0190416_RashmiBoppudi_NYCSchools

class FetchSchoolPresenterTests: XCTestCase {
    var presenter: FetchSchoolPresenter!
    var view = FetchSchoolMockedView()
    var interactor = FechSchoolInteractorMock()
    override func setUp() {
        presenter = FetchSchoolPresenter()
        presenter.view = view
        presenter.interactor = interactor
        interactor.presenter = presenter
    }

    func testFetchSchools() {
        presenter.fetchSchools()
        XCTAssertTrue(view.isFetchedSchoolsCalled)
    }

    override func tearDown() {
        presenter = nil
        
    }
}

class FetchSchoolMockedView:PresenterToViewProtocol {
    var isFetchedSchoolsCalled = false
    func loadFetchedSchools(schools: [School]) {
        isFetchedSchoolsCalled = true
    }
    
    func showNoInterNetAvailabilityMessage() {
        
    }
    
    
}

class FechSchoolInteractorMock: PresenterToInteractorProtocol {
    var presenter: InteractorToPresenterProtocol?
    
    func fetchSchools() {
        let school = School(dbn: "02M260", name: "Clinton School Writers & Artists, M.S. 260")
        presenter?.schoolsListFetched(list: [school])
    }
    
    
}
