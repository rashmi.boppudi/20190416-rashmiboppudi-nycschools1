//
//  SchoolScoreInterfaces.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation
protocol SchoolScoreViewToPresenterProtocol: class{
    
    var view: SchoolScorePresenterToViewProtocol? {get set}
    var interactor: SchoolScorePresenterToInteractorProtocol? {get set}
    var router: SchoolScorePresenterToRouterProtocol? {get set}
    func fetchschoolScore(with dbn:String) 
    
}

protocol SchoolScorePresenterToViewProtocol: class{
  
    func updateScores(model:SchoolScore)
     func showNoInterNetAvailabilityMessage()
}

protocol SchoolScorePresenterToRouterProtocol: class {
    static func createModule(for dbn: String)-> SchoolScoreDetailsViewController
    
}

protocol SchoolScorePresenterToInteractorProtocol: class {
    var presenter:SchoolScoreInteractorToPresenterProtocol? {get set}
    func fetchschoolScore(with dbn:String)
}

protocol SchoolScoreInteractorToPresenterProtocol: class {
    func displayScoreDetails(scoreModel:SchoolScore)
     func showNoInterNetAvailabilityMessage()
    
}
