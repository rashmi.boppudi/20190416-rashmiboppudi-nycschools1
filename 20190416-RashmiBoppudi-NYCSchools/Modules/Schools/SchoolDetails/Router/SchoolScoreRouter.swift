//
//  SchoolScoreRouter.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import UIKit

class SchoolScoreRouter: SchoolScorePresenterToRouterProtocol {
    static func createModule(for dbn: String) -> SchoolScoreDetailsViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "scoreDetails") as! SchoolScoreDetailsViewController
        view.dbn = dbn
        
        let presenter: SchoolScoreViewToPresenterProtocol & SchoolScoreInteractorToPresenterProtocol = SchoolScorePresenter()
        let interactor: SchoolScorePresenterToInteractorProtocol = SchoolScoreInteractor()
        let router:SchoolScorePresenterToRouterProtocol = SchoolScoreRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        return view
    }
    
 
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }

}
