//
//  SchoolModel.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation

struct School:Codable {
    var dbn:String
    var schoolName:String
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
    }
    
    init(dbn: String, name: String) {
        self.dbn = dbn
        self.schoolName = name
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let dbn = try values.decode(String.self, forKey: .dbn)
        let name = try values.decode(String.self, forKey: .schoolName)
        self.init(dbn: dbn, name:  name)

    }
}
