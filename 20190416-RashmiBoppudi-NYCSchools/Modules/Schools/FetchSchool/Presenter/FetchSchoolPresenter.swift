//
//  FetchSchoolPresenter.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation
class FetchSchoolPresenter:ViewToPresenterProtocol {
    func showDetailsTapped(for dbn: String) {
        router?.navigateToSchoolDetails(for: dbn)
    }
    
    var schoolsList:[School] = [School]()
    func fetchSchools() {
        interactor?.fetchSchools()
    }
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    var view:PresenterToViewProtocol?
    

    
    
}

extension FetchSchoolPresenter:InteractorToPresenterProtocol {
    func showNoInterNetAvailabilityMessage() {
        view?.showNoInterNetAvailabilityMessage()
    }
    
    func schoolsListFetched(list: [School]) {
        schoolsList = list
        view?.loadFetchedSchools(schools: schoolsList)
    }
    

}
