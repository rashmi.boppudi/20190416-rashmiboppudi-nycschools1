//
//  SchoolScoreInteractor.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation
import Alamofire
class SchoolScoreInteractor: SchoolScorePresenterToInteractorProtocol {
    var presenter: SchoolScoreInteractorToPresenterProtocol?
    
    func fetchschoolScore(with dbn:String) {
        if Reach().isNetworkReachable() {
            Alamofire.request("https://data.cityofnewyork.us/resource/f9bf-2cp4.json").responseData { (response) in
                let decoder = JSONDecoder()
                let scoreModel = try? decoder.decode([SchoolScore].self, from: response.data!)
                
                if let _scoreModel = scoreModel {
                    let requiredSchool = _scoreModel.filter{$0.dbn == dbn}
                    print(requiredSchool.count)
                    if let neededScoreModel = requiredSchool.first {
                       self.presenter?.displayScoreDetails(scoreModel: neededScoreModel)
                    }
                    
                }
                
            }
            
        }else {
            self.presenter?.showNoInterNetAvailabilityMessage()
        }
    }
    
   
}
