//
//  ViewController.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import UIKit
import SwiftSpinner

class FetchSchoolViewController: UITableViewController {
  var presenter:ViewToPresenterProtocol?
    var schoolsList:[School] = [School]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SwiftSpinner.useContainerView(view)
        SwiftSpinner.show("Please wait loading schools....")
        presenter?.fetchSchools()
    }


}

extension FetchSchoolViewController:PresenterToViewProtocol {
    func showNoInterNetAvailabilityMessage() {
        SwiftSpinner.hide()
        showAlert(title: "No Internet", message: "Please Check you internet connection and try again", actionTitle: "Okay")
    }
    
    func loadFetchedSchools(schools: [School]) {
        schoolsList = schools
        SwiftSpinner.hide()
        tableView.reloadData()
    }
    
    func showAlert(title:String,message:String,actionTitle:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

extension FetchSchoolViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "school", for: indexPath) as! SchoolInfoCell
        let school = schoolsList[indexPath.row]
        cell.configureCell(school: school)
        return cell
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let dbn = schoolsList[indexPath.row].dbn
        presenter?.showDetailsTapped(for: dbn)
    }
    
}
