//
//  FetchSchoolProtocols.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import UIKit
protocol ViewToPresenterProtocol: class{
    
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func fetchSchools()
    func showDetailsTapped(for dbn:String)

}

protocol PresenterToViewProtocol: class{
    func loadFetchedSchools(schools:[School])
    func showNoInterNetAvailabilityMessage()
    
    
}

protocol PresenterToRouterProtocol: class {
    static func createModule()-> FetchSchoolViewController
    func navigateToSchoolDetails(for dbn:String)
    var navigationController:UINavigationController?{get set}
    
    
}

protocol PresenterToInteractorProtocol: class {
   var presenter:InteractorToPresenterProtocol? {get set}
    func fetchSchools()
}

protocol InteractorToPresenterProtocol: class {
    func schoolsListFetched(list:[School])
    func showNoInterNetAvailabilityMessage()
}
