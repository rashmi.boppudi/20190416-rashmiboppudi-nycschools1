//
//  FetchSchoolsRouter.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import UIKit

class FetchSchoolRouter: PresenterToRouterProtocol {
    var navigationController: UINavigationController?
    
    
    func navigateToSchoolDetails(for dbn: String) {
        _navigateToDetails(for: dbn)
    }
    
   
    private func _navigateToDetails(for dbn: String) {
        let scorDetailsVC = SchoolScoreRouter.createModule(for: dbn)
        
        navigationController?.pushViewController(scorDetailsVC, animated: true)
        
    }
    
    static func createModule() -> FetchSchoolViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "searchSchool") as! FetchSchoolViewController
        
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = FetchSchoolPresenter()
        let interactor: PresenterToInteractorProtocol = FetchSchoolInteractor()
        let router:PresenterToRouterProtocol = FetchSchoolRouter()
        router.navigationController = view.navigationController
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        return view
    }
    
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
}



