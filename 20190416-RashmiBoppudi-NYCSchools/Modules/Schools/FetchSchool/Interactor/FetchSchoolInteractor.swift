//
//  FetchSchoolInteractor.swift
//  20190416-RashmiBoppudi-NYCSchools
//
//  Created by Rashmi Boppudi on 16/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import Foundation
import Alamofire
class FetchSchoolInteractor: PresenterToInteractorProtocol {
    func fetchSchools() {
        if Reach().isNetworkReachable() {
        Alamofire.request("https://data.cityofnewyork.us/resource/s3k6-pzi2.json").responseData { (response) in
                let decoder = JSONDecoder()
            let schools = try? decoder.decode([School].self, from: response.data!)
            if let _schools = schools {
                self.presenter?.schoolsListFetched(list: _schools)
            }

            }
            
        }else {
            self.presenter?.showNoInterNetAvailabilityMessage()
        }
    }
    
  
    
    var presenter: InteractorToPresenterProtocol?
}
