//
//  SchoolScorePresenterTests.swift
//  20190416-RashmiBoppudi-NYCSchoolsTests
//
//  Created by Rashmi Boppudi on 17/04/19.
//  Copyright © 2019 NYC. All rights reserved.
//

import XCTest
@testable import _0190416_RashmiBoppudi_NYCSchools

class SchoolScorePresenterTests: XCTestCase {
    var presenter: SchoolScorePresenter!
    var view = SchoolScoreMockedView()
    var interactor = SchoolScoreInteractorMock()
    override func setUp() {
        presenter = SchoolScorePresenter()
        presenter.view = view
        presenter.interactor = interactor
        interactor.presenter = presenter
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testfetchSchoolScore() {
        presenter.fetchschoolScore(with: "02M260")
        XCTAssertTrue(view.isUpdateScoresCalled)
    }

}

class SchoolScoreMockedView:SchoolScorePresenterToViewProtocol {
    
    var isUpdateScoresCalled = false
    func updateScores(model: SchoolScore) {
          isUpdateScoresCalled = true
    }
    
    func showNoInterNetAvailabilityMessage() {
        
    }
    
    
}

class SchoolScoreInteractorMock: SchoolScorePresenterToInteractorProtocol {
    var presenter: SchoolScoreInteractorToPresenterProtocol?
    
    func fetchschoolScore(with dbn: String) {
        let score = SchoolScore(dbn: "02M260", name: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", numOfSatteSttakers: "29", satCriticalReadingAvgScore: "355", satMathAvgScore: "404", satWritingAvgScore: "363")
        presenter?.displayScoreDetails(scoreModel: score)
        
    }
    

    
    
}
